/*
 * Bryant Hansen
 * javascript temperature client
 */

var debugCache;
var refreshPeriod = 2000; // milliseconds
//var host = "wf3";
//var host = "172.16.6.146";
var host = window.location.hostname;

var reqNum = 0;
var frameNumber = 0;

ws_url = "ws://" + host + ":9082/websocket/temperature_monitor";
var ws = new WebSocket(ws_url);
//var ws = new WebSocket("ws://" + host + ":9082/websocket/temperature_monitor");

/**
 * code from:
 * http://www.html5gamedevs.com/topic/1828-how-to-calculate-fps-in-plain-javascript/
 */
var fps = {
    startTime : 0,
    frameNumber : 0,
    getFPS : function(){
        this.frameNumber++;
        var d = new Date().getTime(),
            currentTime = ( d - this.startTime ) / 1000,
            result = Math.floor( ( this.frameNumber / currentTime ) );

        if( currentTime > 1 ){
            this.startTime = new Date().getTime();
            this.frameNumber = 0;
        }
        return result;

    }
};

var poll = function() {
    //if (!this.running) return;
    if (this.running !== undefined) {
        if (!this.running) {
            this.status.innerHMTL = "Websocket Client Status: Stopped";
            return;
        }
    }
    this.status.innerHMTL = "Websocket Client Status: Running";
    setTimeout(function() {
        if ((reqNum % 30) == 0) {
          ws.send("ws_img_req?history=604800");
        }
        else if (((reqNum - 1) % 7) == 0) {
          ws.send("ws_img_req?history=86400");
        }
        else {
          ws.send("ws_img_req?history=1800");
        }
        reqNum++;
        poll();
    }, refreshPeriod);
};

function toggleRunState() {
    if ((this.running === undefined) || (this.running == true)) {
        console.log("setting running state to false")
        this.running = false;
    }
    else {
        console.log("toggleRunState: initiating run...")
        location.reload();
    }
}

ws.onmessage = function (evt) {
    /// @TODO: evaluate the type of data prior to parsing
    /// @TODO: use try/catch here
    resp = evt.data;
    // console.log("server says '%s'", resp.toString())
    // console.log("server says '%s'", evt.toString())
    // console.log(evt)
    obj = JSON.parse(resp);

    this.clientTime.innerHTML   = new Date().toISOString().replace(/-|:|Z/gi,"").replace(/T|\./,"_").replace(/T|\./g,".");
    this.serverTime.innerHTML   = obj.serverTime;
    this.hostname.innerHTML     = obj.hostname;

    //this.requestMessage.value   = obj.req_mes;
    //this.websocketURL.value     = obj.ws_url;
    this.payloadSize.value      = obj.payload.length;
    this.frameNumberBox.value   = obj.frameNumber;
    this.frameSizeBox.value     = obj.frameSize;
    this.status.value           = obj.status;
    this.frameNumberBox.value   = obj.frameNumber;
    this.frameSizeBox.value     = obj.frameSize;

    if (obj.req_mes == "ws_img_req?history=604800") {

        p_fps = fps.getFPS(this.frameNumberBox).toFixed(1);
        this.refreshTextbox.value = p_fps + " fps";
        bw = obj.payload.length * p_fps;
        if (bw > 1000000) {
            this.bandwidth.value      = Math.round(bw / 1000000) + " MBps";
        } else {
            this.bandwidth.value      = Math.round(bw / 1000) + " kBps";
        }
        this.temperature.value = obj.temperature;
        imgSrc = "data:image/" + obj.datatype + ";base64," + obj.payload;

        ctx = this.myCanvas.getContext("2d");
        this.myCanvas.width = 780;
        this.myCanvas.height = 300;
        var websocketImage = new Image();
        websocketImage.width = 720;
        websocketImage.height = 300;
        //ctx.height = 200;
        //ctx.width = 640;
        //ctx.drawImage(websocketImage, 0, 0);

        websocketImage.onload = function() {
            ctx.drawImage(websocketImage, -5, -5, 770, 300, 0, 0, 770, 300);
        };
        websocketImage.src = imgSrc;
    }
    else if (obj.req_mes == "ws_img_req?history=86400") {
        imgSrc2 = "data:image/" + obj.datatype + ";base64," + obj.payload;
        ctx2 = this.myCanvas2.getContext("2d");
        this.myCanvas2.width = 780;
        this.myCanvas2.height = 300;
        var websocketImage2 = new Image();
        websocketImage2.width = 720;
        websocketImage2.height = 300;
        websocketImage2.onload = function() {
            ctx2.drawImage(websocketImage2, -5, -5, 770, 300, 0, 0, 770, 300);
        };
        websocketImage2.src = imgSrc2;
    }
    else if (obj.req_mes == "ws_img_req?history=1800") {
        imgSrc3 = "data:image/" + obj.datatype + ";base64," + obj.payload;
        ctx3 = this.myCanvas3.getContext("2d");
        this.myCanvas3.width = 780;
        this.myCanvas3.height = 300;
        var websocketImage3 = new Image();
        websocketImage3.width = 720;
        websocketImage3.height = 300;
        websocketImage3.onload = function() {
            ctx3.drawImage(websocketImage3, -5, -5, 770, 300, 0, 0, 770, 300);
        };
        websocketImage3.src = imgSrc3;
    }
    else if (obj.req_mes == "http_img_req") {
        // console.log("server says '%s', message source: '%s'", val.toString(), evt.source.toString())
        this.graphImg.src = "/temperature/graph.png"+"?time=" + new Date().getTime();
    }
    else {
        console.log("error: unknown request: '%s'", obj.req_mes)
        this.status.value = "error: unknown request: '" + obj.req_mes + "'";
    }
};

ws.onopen = function() {

    //ws.send("Hello, world");
    this.graphImg = document.getElementById('temp_graph');

    //ws.send("init");

    this.serverTime = document.getElementById('serverTime');
    this.clientTime = document.getElementById('clientTime');

    // this.requestMessage  = document.getElementById('req_mes');
    // this.websocketURL  = document.getElementById('ws_url');
    this.payloadSize  = document.getElementById('payloadSize');
    this.frameNumberBox   = document.getElementById('frameNumber');
    this.refreshTextbox = document.getElementById('refresh');
    this.hostname = document.getElementById("hostname");
    this.frameSizeBox = document.getElementById("frameSize");
    this.bandwidth = document.getElementById("bandwidth");
    this.temperature = document.getElementById("temperature");
    this.timespan_hr = document.getElementById("timespan_hr");
    this.timespan_min = document.getElementById("timespan_min");
    this.endoffset = document.getElementById("endoffset");

    this.status = document.getElementById("status");

    this.myCanvas = document.getElementById("myCanvas");
    this.myCanvas2 = document.getElementById("myCanvas2");
    this.myCanvas3 = document.getElementById("myCanvas3");

    this.status.innerHTML = "Websocket Client Status: Loaded";
    this.running = true;
    // this.status.innerHMTL = "Websocket Client Status: Running";
    poll();

};
