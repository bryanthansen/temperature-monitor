#!/usr/bin/env python3
# Bryant Hansen
# Description:
# Serves temperature data from an rrd

import sys
import os
import logging
import rrdtool
#import json
#import platform
#import base64

from datetime import datetime

rrd_path = "%s/temperature.rrd" % (os.path.dirname(os.path.abspath(__file__)))
colors = ["#FF0000", "#00FF00", "#0000FF", "#800080"]


def gen_graph(img, period):
    if period < 7200:
        title = '%d-second Temperature Window - %s' % (
                      period, datetime.now().strftime("%Y%m%d_%H%M%S.%f")[0:-3]
        )
    else:
        title = '%d-hour Temperature Window - %s' % (
                      period / 3600,
                      datetime.now().strftime("%Y%m%d_%H%M%S.%f")[0:-3]
        )
    args = [
              img,
              '--imgformat', 'PNG',
              '--width', '640',
              '--height', '200',
              '--start', 'now - %d sec' % period,
              '--end', 'now',
              '--vertical-label', '\nTemperature degC\n(uncalibrated)',
              '--title', title,
    ]
    try:
        dev_dict = rrdtool.lastupdate(rrd_path)
    except rrdtool.OperationError as e:
        logging.error("rrdtool.OperationalError exception: %s while getting lastupdate; skip this function" % e)
        return 1
    cnt = 0
    for dev in dev_dict['ds']:
        args.append('DEF:%s=%s:%s:AVERAGE' % (dev, rrd_path, dev))
        args.append('LINE1:%s%s:%s' % (dev, colors[cnt % 3], dev))
        cnt = cnt + 1
    try:
        rrdtool.graph(*args)
        return 0
    except:
        logging.error("Exception %s generating graph" % sys.exc_info())
        return 1


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s',
                        level=logging.DEBUG)
    if len(sys.argv) < 3:
        logging.error("This script requires 2 arguments")
    else:
        logging.info("Calling gen_graph with args %s and %s" % (sys.argv[1], sys.argv[2]))
        gen_graph(sys.argv[1], int(sys.argv[2]))
