#!/usr/bin/env python3
# Bryant Hansen
"""
Find, poll, and log all temperature sensors on the 1-wire bus to an rrd database
"""

import sys
import os
import time
import rrdtool
import logging
from glob import glob

rrd_path = "%s/temperature.rrd" % (os.path.dirname(os.path.abspath(__file__)))
polling_interval_s = 0
iterations = 0


def read_sensor(w1_sys_path):
    """
    Read and parse sensor data file
    """
    try:
        with open(w1_sys_path, "r") as infile:
            data = infile.read()
            temp = data.split('\n')[1].split('=')[1]
            t2 = float(temp) / 1000
    except:
        t2 = 0.0
    return t2

def read_sensor_data(devices):
    """
    Read sensor data and insert it into an rrd database
    """
    data = 'N'
    for path in devices:
        if len(path) < 2:
            continue
        data += ':'
        ftemp = read_sensor(path)
        data += str(ftemp)
        time.sleep(0.25)
    #insert data into round-robin-database
    try:
        rrdtool.update(rrd_path, data)
        logging.info("updated rrd db with '%s'", data)
    except rrdtool.OperationalError as e:
        logging.error("rrdtool OperationalError exception: %s; continue on and hope for the best" % e)

def create_rrd(path, devices):
    '''
    Create an rrd database
    '''
    logging.info("Creating an rrd database for %d devices", len(devices))
    create_rrd_cmd = []
    create_rrd_cmd.append(path)
    create_rrd_cmd.append("--step")
    create_rrd_cmd.append("1")
    for dev in devices:
        dpath = os.path.dirname(dev)
        devid = os.path.basename(dpath)
        create_rrd_cmd.append("DS:%s:GAUGE:10:-40:80" % devid)
    create_rrd_cmd.append("RRA:LAST:0.5:1:5000000")
    logging.info("create rrd cmd: %s" % create_rrd_cmd)
    rrdtool.create(create_rrd_cmd)

def poll_sensors(devices, polling_interval_s, iterations):
    """
    The polling loop
    Currently limited to some iterations
    """
    if not os.path.exists(rrd_path):
        create_rrd(rrd_path, devices)
    if not os.path.exists(rrd_path):
        logging.error('failed to create rrd %s', rrd_path)
        return False
    i = 0
    while True:
        read_sensor_data(devices)
        i += 1
        if iterations > 0 and i > iterations:
            break
        if polling_interval_s > 0:
            time.sleep(polling_interval_s)


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s',
                        level=logging.DEBUG)
    devices = glob("/sys/bus/w1/devices/*/w1_slave")
    if len(devices) < 1:
        logging.error("No 1-wire devices found")
    else:
        logging.info("discovered the following devices: %s", devices)
        poll_sensors(devices, polling_interval_s, iterations)
