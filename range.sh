#!/bin/bash

FORMAT="%Y%m%d_%H%M%S %z"
FORMAT="%Y-%m-%d %H:%M:%S %Z"

FILE=temperature.rrd

echo -n "  First Date: "
date -d @$(rrdtool first "$FILE") "+$FORMAT"

echo -n "   Last Date: "
date -d @$(rrdtool last "$FILE") "+$FORMAT"
