#!/usr/bin/env python3
# Bryant Hansen
# Description:
# Serves temperature data from an rrd

import sys
import os
import logging
import rrdtool
import json
import platform
import base64

from datetime import datetime
from bottle import Bottle, request, abort, route, run, template, static_file

from gevent.pywsgi import WSGIServer
from geventwebsocket import WebSocketError
from geventwebsocket.handler import WebSocketHandler


LISTEN_IP = "0.0.0.0"
PORT = 9082
rrd_path = "%s/temperature.rrd" % (os.path.dirname(os.path.abspath(__file__)))
server_root = os.getcwd()
data_dir = server_root + 'data/'
frameNumber = 0
colors = ["#FF0000", "#00FF00", "#0000FF", "#800080"]
default_period = 24 * 3600;

app = Bottle()

static_files = [ 'style.css', 'client.js', 'index.html' ]
@app.route('/static/<filename>')
def server_static(filename):
    if filename in static_files:
        return static_file('static/%s' % filename, root=server_root)
    else:
        logging.warn('/static/%s not found' % filename)

@app.route('/')
@app.route('/index.html')
def index_html():
    logging.info('returning index.html')
    return server_static('index.html')

@app.route('/get_temp')
def get_temp():
    try:
        resp_dict = {
            'req_mes': 'gettemp',
            'hostname': platform.node(),
            'serverTime': str(datetime.now().strftime("%Y%m%d_%H%M%S.%f")[0:-3])
        }
        dev_dict = rrdtool.lastupdate(rrd_path)
        for key, value in dev_dict['ds'].items():
            resp_dict[key] = '%5.1f degC' % value
        return json.dumps(resp_dict)
    except:
        logging.error("Exception in get_temp: %s", sys.exc_info())
        logging.error("dev_dict: %s", str(dev_dict))
        logging.error("resp_dict: %s", str(resp_dict))
        return 'exception getting temperture'

def gen_graph(img, period):
    if period > 3600 * 24 * 5:
        title = '%d-day Temperature Window - %s' % (
                      period / 3600 / 24, datetime.now().strftime("%Y%m%d_%H%M%S.%f")[0:-3]
        )
    elif period > 3600 * 2:
        title = '%d-hour Temperature Window - %s' % (
                      period / 3600,
                      datetime.now().strftime("%Y%m%d_%H%M%S.%f")[0:-3]
        )
    else:
        title = '%d-second Temperature Window - %s' % (
                      period, datetime.now().strftime("%Y%m%d_%H%M%S.%f")[0:-3]
        )
    args = [
              img,
              '--imgformat', 'PNG',
              '--width', '640',
              '--height', '200',
              '--start', 'now - %d sec' % period,
              '--end', 'now',
              '--vertical-label', '\nTemperature degC\n(uncalibrated)',
              '--title', title,
    ]
    try:
        dev_dict = rrdtool.lastupdate(rrd_path)
    except rrdtool.OperationalError as e:
        logging.error("rrdtool.OperationalError exception: %s while getting lastupdate; skip this function" % e)
        return 1
    cnt = 0
    for dev in dev_dict['ds']:
        args.append('DEF:%s=%s:%s:AVERAGE' % (dev, rrd_path, dev))
        args.append('LINE1:%s%s:%s' % (dev, colors[cnt % 3], dev))
        cnt = cnt + 1
    try:
        rrdtool.graph(*args)
        return 0
    except:
        logging.error("Exception %s generating graph" % str(sys.exc_info()))
        return 1

@app.route('/temperature/graph.png')
def temperature_graph_file():
    if not os.path.exists(rrd_path):
        return False
    img = '/tmp/temperature.png'
    gen_graph(img, default_period)
    return static_file(os.path.basename(img), root=os.path.dirname(img))

@app.route('/websocket')
@app.route('/websocket/<ws_url>')
def handle_websocket(ws_url='default_ws_url'):
    global frameNumber
    wsock = request.environ.get('wsgi.websocket')
    if not wsock:
        logging.error("Expected WebSocket request.")
        abort(400, 'Expected WebSocket request.')
    while True:
        try:
            req_mes = wsock.receive()
            logging.debug("handle_ws_url: '%s', request: '%s'", ws_url, req_mes)
            frameNumber = frameNumber + 1
            try:
                temperature = rrdtool.lastupdate(rrd_path)['ds']
            except rrdtool.OperationalError as e:
                temperature = "OperationalErrorException"
                logging.error("rrdtool.OperationalError exception: %s while getting lastupdate; skip this function" % e)
            resp_dict = {
                'ws_url': ws_url,
                'req_mes': req_mes,
                'frameNumber': str(frameNumber),
                'frameSize': '640x200',
                'payload': '',
                'datatype': 'unspecified',
                'status': 'unspecified',
                'temperature': temperature,
                'hostname': platform.node(),
                'serverTime': str(datetime.now().strftime("%Y%m%d_%H%M%S.%f")[0:-3])
            }
            if ws_url == 'temperature_monitor':
                period = default_period
                if req_mes.startswith('ws_img_req?'):
                    req_mes_split = req_mes.split('?')
                    req_mes = req_mes_split[0]
                    if len(req_mes_split) < 2:
                        logging.warning("request message with params received, but no params found: %s", str(req_mes_split))
                    else:
                        params = req_mes_split[1]
                        logging.info("%s: message: %s, params: %s",
                                      ws_url, req_mes, str(params))
                        for param in params.split('&'):
                            var = param.split('=')[0]
                            val = param.split('=')[1]
                            if var == 'history':
                                myval = int(val)
                                if myval > 0:
                                    period = int(val)
                                    logging.info("period set to %d seconds", period)
                                else:
                                    logging.error(
                                        "variable %s value set to %s; must be a number > 0",
                                        var, val)

                if req_mes == 'ws_img_req':
                    img = '/tmp/temperature.png'
                    gen_graph(img, period)

                    # send an image file over a websocket
                    f = open(img, 'rb')
                    resp_dict['datatype'] = 'image/png'
                    resp_dict['payload'] = base64.b64encode(f.read()).decode()
                    f.close()
                    logging.info("%s:%s: produced %d-byte base64 response",
                                  ws_url, req_mes, len(resp_dict['payload']))
                    response = json.dumps(resp_dict)
                    wsock.send(response)

                elif req_mes == 'http_img_req':
                    resp_dict['payload'] = "get_response"
                    response = json.dumps(resp_dict)
                    logging.debug("returning message '%s'", response)
                    wsock.send(response)

                else:
                    logging.warning("unknown req_mes: '%s'", req_mes)
                    resp_dict['payload'] = "error"
                    response = json.dumps(resp_dict)
                    wsock.send(response)

            else:
                resp_dict['payload'] = "error: unknown url: %s" % ws_url
                response = json.dumps(resp_dict)
                logging.debug("unknown URL: '%s', returning '%s'", ws_url, response)
                wsock.send(response)

        except WebSocketError:
            logging.debug("exception: WebSocketError")
            break


def run(host, port, debug):
    if debug:
        logging.level = logging.DEBUG
    logging.info('launching WSGIServer on %s:%d' % (LISTEN_IP, PORT))
    server = WSGIServer((host, port), app, handler_class=WebSocketHandler)
    server.serve_forever()


#####################################################
# Main

if __name__ == "__main__":
    USAGE = sys.argv[0] + "  command args"

    logging.basicConfig(
        format='%(asctime)s:%(levelname)s:%(funcName)s:%(message)s',
        level=logging.INFO)

    run(host=LISTEN_IP, port=PORT, debug=True)

